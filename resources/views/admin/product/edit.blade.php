@extends('Admin.layouts.app')
@inject('category','App\Models\Catogery')
@section('title')
	تعديل : المنتجات
@endsection
@section('topBar')
	<li class="m-menu__item">
		<a href="{{url('admincp')}}" class="m-menu__link">
			<span class="m-menu__link-text">الرئيسية</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item">
		<a href="{{url('admincp/products')}}" class="m-menu__link">
			<span class="m-menu__link-text">التحكم بالمنتجات</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item" style="background: #e0deff;">
		<a href="javascript:;" class="m-menu__link">
			<span class="m-menu__link-text">المنتجات</span>
			<i class="m-menu__hor-arrow la la-angle-down"></i>
		</a>
	</li>
@endsection

@section('header')
@endsection

@section('content')
	<!--begin::Portlet-->
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text">
						تعديل المنتجات
					</h3>
				</div>
			</div>
		</div>

	{!!Form::model($product,[
'action'=>['Admin\ProductController@update',$product->id],
'method'=>'put',
'files'=>true,
'class'=>'m-form m-form--fit m-form--label-align-right'
]) !!}






		<div class="col-lg-12">
			<label class=" col-form-label">   اسم  عربي</label>

			<div class="col-lg-9">


				{!!Form::text('name[ar]',getJsonData($product->name,'ar'),[
                'class'=>'form-control m-input','placeholder'=>" اسم  عربي"
                ])!!}
			</div>
		</div>


		<div class="col-lg-12">
			<label class="col-form-label">   اسم  انجليزي</label>

			<div class="col-lg-9">


				{!!Form::text('name[en]',getJsonData($product->name,'en'),[
                'class'=>'form-control m-input','placeholder'=>" اسم  انجليزي"
                ])!!}
			</div>
		</div>



		<div class="col-lg-12">
			<label class=" col-form-label">   تفاصيل  عربي</label>

			<div class="col-lg-9">


				{!!Form::text('details[ar]',getJsonData($product->details,'ar'),[
                'class'=>'form-control m-input','placeholder'=>"  تفاصيل عربي"
                ])!!}
			</div>
		</div>


		<div class="col-lg-12">
			<label class="col-form-label">    تفاصيل انجليزي</label>

			<div class="col-lg-9">


				{!!Form::text('details[en]',getJsonData($product->details,'en'),[
                'class'=>'form-control m-input','placeholder'=>"  تفاصيل انجليزي"
                ])!!}
			</div>
		</div>




		<div class="col-lg-12">
			<label class="col-form-label">  النوع </label>

			<div class="col-lg-9">


				{!!Form::text('type',$product->type,[
                'class'=>'form-control m-input','placeholder'=>"   النوع"
                ])!!}
			</div>
		</div>



		<div class="col-lg-12">
			<label class="col-form-label">السعر</label>

			<div class="col-lg-9">


				{!!Form::text('price',$product->price,[
                'class'=>'form-control m-input','placeholder'=>" السعر"
                ])!!}
			</div>
		</div>



		<div class="col-lg-12">
			<label class="col-form-label">   التخفيض</label>

			<div class="col-lg-9">


				{!!Form::text('discount_value',$product->discount_value,[
                'class'=>'form-control m-input','placeholder'=>" النص  انجليزي"
                ])!!}
			</div>
		</div>


		<div class="col-lg-12">

			<div class="col-lg-9">

				<label class=" col-form-label"> اسم الصنف</label>



				@php $cats=App\Models\Catogery::all();	@endphp


				<select name='catogery_id' class="col-lg-12 form-control">
					@foreach($cats as $cat)


						<option value="{{$cat->id}}"> {{getJsonData($cat->name,'ar')}} </option>



					@endforeach



				</select>





			</div>
		</div>



		<div class="col-lg-12">
			<div class="col-lg-9">

				<label class=" col-form-label">   العرض</label>
				@php $offers=App\Models\Offer::all();	@endphp


				<select name='offer_id' class="col-lg-12 form-control">
					@foreach($offers as $offer)


						<option value="{{$offer->id}}"> {{getJsonData($offer->title,'ar')}} </option>



					@endforeach



				</select>
			</div>

		</div>


		<div class="col-lg-12">
			<label class="col-form-label">   الصوره الفرعيه</label>

			<div class="col-lg-9">

				<input type="file" id="file" multiple="multiple" class="file-input form-control" accept="image/*"
					   name="images[]">


			</div>
		</div>


		<div class="col-lg-12">
			<label class="col-form-label">   الصوره الرئيسية</label>

			<div class="col-lg-9">

				<input type="file" id="file" multiple="multiple" class="file-input form-control" accept="image/*"
					   name="image">


			</div>
		</div>




		<br><br>
	<br>
	<div class='form-group' >
		<br>
		<button class="btn btn-primary" type="submit"> submit</button>
	</div>

	</div>

	</div>

	</div>
	</div>


		{!! Form::close()!!}
	<!--end::Form-->
	</div>
	<!--end::Portlet-->
@endsection
@section('footer')
<script type="text/javascript">

</script>
@endsection

