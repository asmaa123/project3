@extends('Admin.auth.app')

@section('content')
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-login m-login--forget-password  m-login--5" id="m_login"
             style="background-image: url({{Request::root()}}/admin//app/media/img//bg/bg-3.jpg);">
            <div class="m-login__wrapper-1 m-portlet-full-height">
                <div class="m-login__wrapper-1-1">
                    <div class="m-login__contanier">
                        <div class="m-login__content">
                            <div class="m-login__logo">
                                <a href="#">
                                    <img src="{{Request::root()}}/admin/app/media/img/logos/logo-2.png">
                                </a>
                            </div>
                            <div class="m-login__title">
                                <h3>إسترجاع كلمة المرور</h3>
                            </div>
                            <div class="m-login__desc">
                                أتبع الخطوات لإسترجاع كلمة المرور
                            </div>
                        </div>
                    </div>
                    <div class="m-login__border">
                        <div></div>
                    </div>
                </div>
            </div>
            <div class="m-login__wrapper-2 m-portlet-full-height">
                <div class="m-login__contanier">
                    <div class="m-login__forget-password">
                        <div class="m-login__head">
                            <h3 class="m-login__title">هل نسيت كلمة المرور ؟</h3>
                            <div class="m-login__desc">أكتب بريدك الإلكترونى لإسترجاع كلمة المرور :</div>
                        </div>
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form class="m-login__form m-form" action="{{ route('admin.password.email') }}" method="post">
                            @csrf
                            <div class="form-group m-form__group">
                                <input id="email" type="email" class="form-control m-input{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="أدخل بريدك الإلكترونى" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="m-login__form-action">
                                <button type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">أسترجاع كلمة المرور
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection