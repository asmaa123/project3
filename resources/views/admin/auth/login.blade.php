@extends('Admin.auth.app')

@section('content')
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-login m-login--sing-in  m-login--5" id="m_login"
             style="background-image: url({{Request::root()}}/admin//app/media/img//bg/bg-3.jpg);">
            <div class="m-login__wrapper-1 m-portlet-full-height">
                <div class="m-login__wrapper-1-1">
                    <div class="m-login__contanier">
                        <div class="m-login__content">
                            <div class="m-login__logo">
                                <a href="#">
                                    <img src="{{Request::root()}}/admin/app/media/img/logos/logo-2.png">
                                </a>
                            </div>
                            <div class="m-login__title">
                                <h3>أهلا بك فى لوحة التحكم</h3>
                            </div>
                            <div class="m-login__desc">
                                أدخل البيانات لإتمام عمليه الدخول
                            </div>
                        </div>
                    </div>
                    <div class="m-login__border">
                        <div></div>
                    </div>
                </div>
            </div>
            <div class="m-login__wrapper-2 m-portlet-full-height">
                <div class="m-login__contanier">
                    <div class="m-login__signin">
                        <div class="m-login__head">
                            <h3 class="m-login__title">تسجيل الدخول للوحة التحكم</h3>
                            <?php print_r($errors); ?>
                        </div>
                        <form class="m-login__form m-form" method="POST" action="{{ route('admin.login') }}">
                            @csrf
                            <div class="form-group m-form__group">
                                <input class="form-control m-input{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" placeholder="البريد الإلكترونى" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="form-group m-form__group">
                                <input class="form-control m-input m-login__form-input--last{{ $errors->has('password') ? ' is-invalid' : '' }}" type="Password" placeholder="كلمة المرور" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                @endif
                            </div>
                            <div class="row m-login__form-sub">
                                <div class="col m--align-left">
                                    <label class="m-checkbox m-checkbox--focus">
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> تذكرنى
                                        <span></span>
                                    </label>
                                </div>
                                @if (Route::has('password.request'))
                                    <div class="col m--align-right">
                                        <a href="{{ route('admin.password.request') }}" class="m-link">هل نسيت كلمة المرور ؟</a>
                                    </div>
                                @endif
                            </div>
                            <div class="m-login__form-action">
                                <button type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">تسجيل الدخول</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection