@extends('admin.layouts.app')
@section('title')
    الرئيسية
@endsection

@section('header')

@endsection
@inject('trans','App\Http\Controllers\Admin\HomeController')
@section('megaMenu')
    <div class="hor-menu hidden-sm hidden-xs">
        <ul class="nav navbar-nav">
            <!-- DOC: Remove data-hover="megamenu-dropdown" and data-close-others="true" attributes below to disable the horizontal opening on mouse hover -->
            <li class="classic-menu-dropdown active" aria-haspopup="true">
                <a> رئيسية لوحة التحكم
                    <span class="selected"> </span>
                </a>
            </li>
            <li class="mega-menu-dropdown" aria-haspopup="true">
                <a href="{{url('/')}}" target="_blank"> صفحة الموقع الرئيسية > </a>
            </li>
        </ul>
    </div>
@endsection

@section('content')
<div class="m-content">
        <div class="note note-info">
        <!-- BEGIN DASHBOARD STATS -->
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat green-haze">
                    <div class="visual">
                        <i class="fa fa-user fa-3x"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            {{\App\Models\User::where('id','!=',0)->count()}}
                        </div>
                        <div class="desc">
                            إجمالى الأعضاء
                        </div>
                    </div>
                    <a class="more" href="{{url('admincp/user')}}">
                        عرض المزيد
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat red-intense">
                    <div class="visual">
                        <i class="fa flaticon-web fa-3x"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            {{App\Models\Product::count()}}
                        </div>
                        <div class="desc">
                            اجمالي المنتجات
                        </div>
                    </div>
                    <a class="more" href="{{url('admincp/products')}}">
                        عرض المزيد
                    </a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="dashboard-stat blue-madison">
                    <div class="visual">
                        <i class="fa flaticon-shapes
 fa-3x"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            {{\App\Models\Catogery::count()}}
                        </div>
                        <div class="desc">
                            إجمالى الإقسام
                        </div>
                    </div>
                    <a class="more" href="{{url('admincp/catogeries')}}">
                        عرض المزيد
                    </a>
                </div>
            </div>
         
        </div>
    </div>
</div>
    <!-- END DASHBOARD STATS -->
@endsection

@section('footer')

@endsection

    <!--BEGIN TABS--
    <!--END TABS-->

    <!-- END PORTLET-->

