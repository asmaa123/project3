@extends('Admin.layouts.app')

@section('title')
	تعديل كلمة المرور
@endsection
@section('topBar')
	<li class="m-menu__item">
		<a href="{{url('admincp')}}" class="m-menu__link">
			<span class="m-menu__link-text">الرئيسية</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item" style="background: #e0deff;">
		<a href="javascript:;" class="m-menu__link">
			<span class="m-menu__link-text">تعديل كلمة المرور</span>
			<i class="m-menu__hor-arrow la la-angle-down"></i>
		</a>
	</li>
@endsection

@section('header')
@endsection

@section('content')
	<!--begin::Portlet-->
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text">
						تعديل كلمة المرور
					</h3>
				</div>
			</div>
		</div>
		<!--begin::Form-->
		{!! Form::model(['url' => 'admincp/change-password','method'=> 'post','class'=>'m-form m-form--fit m-form--label-align-right']) !!}
			<div class="m-portlet__body">
				<div class="form-group m-form__group row m-form__section--last">
					<label class="col-lg-1 col-form-label">الحاليه : </label>
					<div class="col-lg-3{{ $errors->has('curPassword') ? ' has-danger' : '' }}">
				    	{!! Form::password('curPassword',['class'=>'form-control m-input','placeholder'=>"كلمة المرور الحاليه"]) !!}
				        @if ($errors->has('curPassword'))
				            <span class="form-control-feedback" role="alert">
				                <strong>{{ $errors->first('curPassword') }}</strong>
				            </span>
				        @endif
					</div>
					<label class="col-lg-1 col-form-label">الجديده : </label>
					<div class="col-lg-3{{ $errors->has('password') ? ' has-danger' : '' }}">
				    	{!! Form::password('password',['class'=>'form-control m-input','placeholder'=>"كلمة المرور الجديده"]) !!}
				        @if ($errors->has('password'))
				            <span class="form-control-feedback" role="alert">
				                <strong>{{ $errors->first('password') }}</strong>
				            </span>
				        @endif
					</div>
					<label class="col-lg-1 col-form-label">التأكيد : </label>
					<div class="col-lg-3{{ $errors->has('password_confirmation') ? ' has-danger' : '' }}">
				    	{!! Form::password('password_confirmation',['class'=>'form-control m-input','placeholder'=>"تأكيد كلمة المرور"]) !!}
				        @if ($errors->has('password_confirmation'))
				            <span class="form-control-feedback" role="alert">
				                <strong>{{ $errors->first('password_confirmation') }}</strong>
				            </span>
				        @endif
					</div>
				</div>
			</div>
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col-lg-6">
							<button type="submit" class="btn btn-success">تعديل</button>
						</div>
					</div>
				</div>
			</div>
			<br>
		{!! Form::close() !!}
	<!--end::Form-->
	</div>
	<!--end::Portlet-->
@endsection
@section('footer')
<script type="text/javascript">

</script>
@endsection

