@extends('Admin.layouts.app')

@section('title')
	إعدادت الموقع
@endsection

@section('header')
@endsection

@section('megaMenu')
<div class="hor-menu hidden-sm hidden-xs">
    <ul class="nav navbar-nav">
        <!-- DOC: Remove data-hover="megamenu-dropdown" and data-close-others="true" attributes below to disable the horizontal opening on mouse hover -->
        <li class="classic-menu-dropdown" aria-haspopup="true">
            <a href="{{url('/admincp')}}"> رئيسية لوحة التحكم
            </a>
        </li>
        <li class="mega-menu-dropdown active" aria-haspopup="true">
            <a> إعدادت ايقوانات التواصل الإجتماعى
                <span class="selected"> </span>
            </a>
        </li>
    </ul>
</div>
@endsection

@section('pageHeader')
	<div class="page-bar hidden-md hidden-lg">
		<ul class="page-breadcrumb">
			<li>
				<i class="fa fa-home"></i>
				<a href="{{url('/admincp')}}">الرئيسية</a>
				<i class="fa fa-angle-left"></i>
			</li>
          <li>
            <i class="fa fa-home"></i>
            <a>إعدادت ايقوانات التواصل الإجتماعى</a>
          </li>
		</ul>
	</div>

@endsection

@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box red">
                <div class="portlet-title">
                    <div class="caption col-md-9">
                        التحكم بصفحات المتابعة على مواقع التواصل الإجتماعي 
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('admincp/settings') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @foreach($settings as $setting )
                        	@if($setting->type == 30)
                        	<div class="form-group{{ $errors->has('$setting->name') ? ' has-error' : '' }}">
                        	    <label class="col-md-2 col-sm-2 col-xs-12 control-label">{{$setting->slug}}</label>
                        	    <!-- <div class="clearfix"></div> -->
                        	    <div class="col-md-9 col-sm-10 col-xs-12{{ $errors->has('color') ? ' has-error' : '' }}">
                        	        <div class="input-group">
                        	            <span class="input-group-addon">
                        	                @php $checkbOX = $settings->where('type',34)->where('name',$setting->name.'Act')->first() @endphp
                        	                <input type="hidden" name="{{$checkbOX->name}}" value="0" />
                        	                {!! FORM::checkbox($checkbOX->name,1,$checkbOX->value == 1 ? 'checked' : false) !!}
                        	                <span></span>
                        	            </span>
                        	            {!! FORM::text($setting->name, $setting->value ,['class'=>'form-control']) !!}
                        	        </div>
                        	    </div>
                        	</div>
                        	@elseif($setting->type == 33)
                        	<div class="form-group{{ $errors->has('$setting->name') ? ' has-error' : '' }}">
                        	    <label class="col-md-2 control-label">{{$setting->slug}}</label>
                        	    <div class="col-md-9">
                        	        <div class="input-group">
                        	            <span class="input-group-addon">
                        	            <i class="icon-settings"></i>
                        	            </span>
                        	        {!! FORM::select("$setting->name",['1'=>'تفعيل','0'=>'تعطيل'],$setting->value,['class'=>'form-control']) !!}
                        	        </div>
                        	        @if ($errors->has('$setting->name'))
                        	            <span class="help-block">
                        	                <strong>{{ $errors->first('$setting->name') }}</strong>
                        	            </span>
                        	        @endif
                        	    </div>
                        	</div>
                        	@endif
                        @endforeach
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green" id="socialSubmit">حفظ الإعدادات
                                        <i class="fa fa-save"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.content -->

@endsection

<!-- footer -->
@section('footer')

@endsection