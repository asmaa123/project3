@extends('Admin.layouts.app')

@section('title')
	تعديل الاعدادت
@endsection
@section('topBar')
	<li class="m-menu__item">
		<a href="{{url('admincp')}}" class="m-menu__link">
			<span class="m-menu__link-text">الرئيسية</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item">
		<a href="{{url('admincp/settings')}}" class="m-menu__link">
			<span class="m-menu__link-text">التحكم بالاعدادت</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item" style="background: #e0deff;">
		<a href="javascript:;" class="m-menu__link">
			<span class="m-menu__link-text">تعديل الاعدادت</span>
			<i class="m-menu__hor-arrow la la-angle-down"></i>
		</a>
	</li>
@endsection

@section('header')
@endsection

@section('content')
	<!--begin::Portlet-->
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text">
						تعديل الاعدادت
					</h3>
				</div>
			</div>
		</div>

	{!!Form::model($setting,[
'action'=>['Admin\SettingsController@update',$setting->id],
'method'=>'put',
'files'=>true,
'class'=>'m-form m-form--fit m-form--label-align-right'
]) !!}



			<div class="col-lg-12">
			<label class="col-lg-1 col-form-label">فيس بوك</label>

			<div class="col-lg-9">


			{!!Form::text('slug',null,[
            'class'=>'form-control m-input'
            ])!!}
			</div>
			</div>


        <div class="col-lg-12">
            <label class="col-lg-1 col-form-label">الايميل</label>

            <div class="col-lg-9">


                {!!Form::text('name',null,[
                'class'=>'form-control m-input'
                ])!!}
            </div>
        </div>


        <div class="col-lg-12">
            <label class="col-lg-1 col-form-label">الفون</label>

            <div class="col-lg-9">


                {!!Form::text('value',null,[
                'class'=>'form-control m-input'
                ])!!}
            </div>
        </div>


        <div class="col-lg-12">
            <label class="col-lg-1 col-form-label">
                التليجرام
            </label>

            <div class="col-lg-9">


                {!!Form::text('type',null,[
                'class'=>'form-control m-input'
                ])!!}
            </div>
        </div>

        <div class="col-lg-12">
            <label class="col-lg-1 col-form-label">
                واتس
            </label>

            <div class="col-lg-9">


                {!!Form::text('module',null,[
                'class'=>'form-control m-input'
                ])!!}
            </div>
        </div>

			<br>
			<div class='form-group' >
				<br>
				<button class="btn btn-primary" type="submit"> submit</button>
			</div>

		</div>




		{!! Form::close()!!}
	<!--end::Form-->
	</div>
	<!--end::Portlet-->
@endsection
@section('footer')
<script type="text/javascript">

</script>
@endsection

