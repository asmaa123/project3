@extends('Admin.layouts.app')

@section('title')
	عرض المستخدمين
@endsection
@section('topBar')
	<li class="m-menu__item">
		<a href="{{url('admincp')}}" class="m-menu__link">
			<span class="m-menu__link-text">الرئيسية</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item">
		<a href="{{url('admincp/user')}}" class="m-menu__link">
			<span class="m-menu__link-text">التحكم بالمستخدمين</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item" style="background: #e0deff;">
		<a href="javascript:;" class="m-menu__link">
			<span class="m-menu__link-text"></span>
			<i class="m-menu__hor-arrow la la-angle-down"></i>
		</a>
	</li>
@endsection

@section('header')
@endsection

@section('content')
	<!--begin::Portlet-->
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text">
						تعديل بيانات
					</h3>
				</div>
			</div>
		</div>
		<!--begin::Form-->

		{!!Form::model($prods,[
    'action'=>['Admin\UserController@update',$prods->id],
    'method'=>'put',
    'files'=>true,
    'class'=>'m-form m-form--fit m-form--label-align-right'
    ]) !!}



		<div class="col-lg-12">
			<label class=" col-form-label">اسم المستخدم </label>

			<div class="col-lg-9">





				{!!Form::text('name',$prods->name,[
                'class'=>'form-control m-input','placeholder'=>"اسم المستخدم "
                ])!!}
			</div>
		</div>


		<div class="col-lg-12">
			<label class="col-form-label">  الايميل</label>

			<div class="col-lg-9">


				{!!Form::text('email',$prods->email,[
                'class'=>'form-control m-input','placeholder'=>"الايميل "
                ])!!}
			</div>
		</div>

		<div class="col-lg-12">
			<label class="col-form-label">  الفون</label>

			<div class="col-lg-9">


				{!!Form::text('phone',$prods->phone,[
                'class'=>'form-control m-input','placeholder'=>"الفون "
                ])!!}
			</div>
		</div>


		<div class="col-lg-12">
			<label class="col-form-label">  النوع</label>

			<div class="col-lg-9">


				{!!Form::text('gender',$prods->gender,[
                'class'=>'form-control m-input','placeholder'=>"النوع"
                ])!!}
			</div>
		</div>



		<div class="col-lg-12">
			<label class="col-form-label">  التاريخ</label>

			<div class="col-lg-9">


				{!!Form::text('date',$prods->date,[
                'class'=>'form-control m-input','placeholder'=>"التاريخ "
                ])!!}
			</div>
		</div>




		<div class="col-lg-12">
			<label class="col-lg-1 col-form-label"> الصوره</label>


			<div class="col-lg-9">
				{!!Form::file('image',null,[
            'class'=>'form-control m-input'
            ])!!}
			</div>
		</div>





		<br>
		<div class='form-group' >
			<br>
			<button class="btn btn-primary" type="submit"> submit</button>
		</div>


		<!--end::Form-->
	</div>

	{!! Form::close()!!}
	<!--end::Portlet-->
@endsection
@section('footer')
<script type="text/javascript">

</script>
@endsection

