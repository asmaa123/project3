@extends('Admin.layouts.app')
@inject('user','App\Models\User')
@inject('catogery','App\Models\Catogery')
@inject('product','App\Models\Product')

@section('title')
إحصائيات عامه
@endsection

@section('header')
@endsection
@section('content')
مرحبا بك فى رئيسية لوحة التحكم

<br><br><br>
<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head" style="height: auto;background: #e0deff">
            <div class="col-sm-6 col-md-4 col-xs-12">


                <div class="info-box">


                    <span class="info-box-icon by-aqua"><i class="fa fa-user"></i></span>

                    <div class="info-box-content">

				<span class="info-box-text">


					<span class="info-box-number">

					المستخدمين
						{{$user->count()}}


					</span>
				</span>



                    </div>


                </div>


            </div>
            <div class="col-sm-6 col-md-4 col-xs-12">


                <div class="info-box">


                    <span class="info-box-icon by-aqua"><i class="flaticon-grid-menu-v2 fa fa-2x"></i></span>

                    <div class="info-box-content">

				<span class="info-box-text">


					<span class="info-box-number">
						 الاقسام {{$catogery->count()}}


					</span>
				</span>



                    </div>


                </div>


            </div>
            <div class="col-sm-6 col-md-4 col-xs-12">


                <div class="info-box">


                    <span class="info-box-icon by-aqua"><i class="flaticon-suitcase fa fa-2x"></i></span>

                    <div class="info-box-content">

				<span class="info-box-text">


					<span class="info-box-number">

					المنتجات{{$product->count()}}


					</span>
				</span>



                    </div>


                </div>


            </div>

            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">

                </div>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">


    </div>

</div>



@endsection
@section('footer')
@endsection
