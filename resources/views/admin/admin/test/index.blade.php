@extends('Admin.layouts.app')
@section('title','صفحة إختبار أولى')

@section('header')
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
@endsection

@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('admincp')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item active-top-bar">
        <a href="javascript:;" class="m-menu__link">
            <span class="m-menu__link-text">صفحة إختبار</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="javascript:;" class="m-menu__link">
            <span class="m-menu__link-text">صفحة إضافة</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>
@endsection

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        التحكم بالمستخدمين
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_testArea">
                <thead>
                <tr>
                    <th>#</th>
                    <th>الإسم</th>
                    <th>الوظيفه</th>
                    <th>القسم</th>
                    <th>الأدوات</th>
                </tr>
                </thead>
                <tbody>
                @for($i=1; $i<50; $i++)
                    <tr>
                        <td>{{$i}}</td>
                        <td>محمود الشناوى</td>
                        <td>WebDeveloper</td>
                        <td>قسم المشاريع</td>
                        <td>
                            <a href="#" class="btn btn-brand m-btn">تعديل</a>
                            <a href="{{url('admincp/testArea/3/delete')}}" class="btn btn-danger m-btn">حذف</a>
                        </td>
                    </tr>
                @endfor
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('footer')
    {!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}
    {!! Html::script('admin/custom/js/testArea/script.js') !!}
@endsection
