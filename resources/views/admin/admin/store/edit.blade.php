@extends('Admin.layouts.app')

@section('title')
	تعديل : المتجر
@endsection
@section('topBar')
	<li class="m-menu__item">
		<a href="{{url('admincp')}}" class="m-menu__link">
			<span class="m-menu__link-text">الرئيسية</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item">
		<a href="{{url('admincp/stores')}}" class="m-menu__link">
			<span class="m-menu__link-text">التحكم بالمتجر</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item" style="background: #e0deff;">
		<a href="javascript:;" class="m-menu__link">
			<span class="m-menu__link-text">تعديل :المتجر</span>
			<i class="m-menu__hor-arrow la la-angle-down"></i>
		</a>
	</li>
@endsection

@section('header')
@endsection

@section('content')
	<!--begin::Portlet-->
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text">
						تعديل :المتجر
					</h3>
				</div>
			</div>
		</div>

	{!!Form::model($store,[
'action'=>['Admin\StoreController@update',$store->id],
'method'=>'put',
'files'=>true,
'class'=>'m-form m-form--fit m-form--label-align-right'
]) !!}

		<div class="form-group">

			<div class="col-lg-12">
				<label class="col-form-label">اسم المتجر</label>

				<div class="col-lg-9">


					{!!Form::text('namee',null,[
                    'class'=>'form-control m-input','placeholder'=>" اسم المتجر"
                    ])!!}
				</div>
			</div>

		</div>

		<div class="form-group">
		<div class="col-lg-12">
			<div class="col-lg-9">

				<label class=" col-form-label">   اسم الصنف</label>
				@php $catogeries=App\Models\Catogery::all();	@endphp


				<select name='catogery_id' class="col-lg-12 form-control">
					@foreach($catogeries as $catogery)


						<option value="{{$catogery->id}}"> {{getJsonData($catogery->name,'ar')}} </option>



					@endforeach



				</select>
			</div>

		</div>

		</div>

		<div class="form-group">
			<div class="col-lg-12">
				<label class="col-form-label">   الصوره الرئيسية</label>

				<div class="col-lg-9">

					<input type="file" id="file" class="file-input form-control" accept="image/*"
						   name="image">

				</div>
			</div>
		</div>



<br><br>
			<br>
			<div class='form-group' >
				<br>
				<button class="btn btn-primary" type="submit"> submit</button>
			</div>

		</div>




		{!! Form::close()!!}
	<!--end::Form-->
	</div>
	<!--end::Portlet-->
@endsection
@section('footer')
<script type="text/javascript">

</script>
@endsection

