@extends('Admin.layouts.app')
@inject('model','App\Models\Image')
@section('title')
اضافه السليدر
@endsection
@section('topBar')
	<li class="m-menu__item">
		<a href="{{url('admincp')}}" class="m-menu__link">
			<span class="m-menu__link-text">الرئيسية</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item">
		<a href="{{url('admincp/users')}}" class="m-menu__link">
			<span class="m-menu__link-text">التحكم بالسليدر</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item" style="background: #e0deff;">
		<a href="javascript:;" class="m-menu__link">
			<span class="m-menu__link-text"></span>
			<i class="m-menu__hor-arrow la la-angle-down"></i>
		</a>
	</li>
@endsection

@section('header')
@endsection

@section('content')
	<!--begin::Portlet-->
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text">

					</h3>
				</div>
			</div>
		</div>

	{!!Form::model($model,[
'action'=>'Admin\SliderController@store',
'files'=>true,
'class'=>'m-form m-form--fit m-form--label-align-right'
]) !!}


		<div class="col-lg-12">
			<label class="col-lg-1 col-form-label">النص بالعربي</label>


			<div class="col-lg-9">
				{!!Form::textarea('text[ar]',null,[
            'class'=>'form-control m-input',
            'placeholder'=>'النص بالعربي'
            ])!!}
			</div>
		</div>

		<div class="col-lg-12">
			<label class="col-lg-1 col-form-label"> النص انجليزي </label>


			<div class="col-lg-9">
				{!!Form::textarea('text[en]',null,[
            'class'=>'form-control m-input',
            'placeholder'=>'النص بالانجليزي'
            ])!!}
			</div>
		</div>



		<div class="col-lg-12">
			<label class=" col-form-label">عربي العنوان</label>


			<div class="col-lg-9">
				{!!Form::text('title[ar]',null,[
            'class'=>'form-control m-input',
             'placeholder'=>'عربي العنوان'
            ])!!}
			</div>
		</div>



		<div class="col-lg-12">
			<label class=" col-form-label"> انجليزي العنوان</label>


			<div class="col-lg-9">
				{!!Form::text('title[en]',null,[
            'class'=>'form-control m-input',
             'placeholder'=>'انجليزي العنوان'
            ])!!}
			</div>
		</div>



		<div class="col-lg-12">
				<label class="col-lg-1 col-form-label"> الصوره</label>


				<div class="col-lg-9">
					{!!Form::file('image',null,[
                'class'=>'form-control m-input'
                ])!!}
				</div>
			</div>
<br><br>
			<br>
			<div class='form-group' >
				<br>
				<button class="btn btn-primary" type="submit"> submit</button>
			</div>

		</div>




		{!! Form::close()!!}
	<!--end::Form-->
	</div>
	<!--end::Portlet-->
@endsection
@section('footer')
<script type="text/javascript">

</script>
@endsection

