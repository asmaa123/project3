@extends('Admin.layouts.app')

@section('title')
	عرض تفاصيل الطلبات
@endsection
@section('topBar')
	<li class="m-menu__item">
		<a href="{{url('admincp')}}" class="m-menu__link">
			<span class="m-menu__link-text">الرئيسية</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item" style="background: #e0deff;">
		<a href="{{url('admincp\orders')}}" class="m-menu__link">
			<span class="m-menu__link-text">التحكم بالطلبات</span>
			<i class="m-menu__hor-arrow la la-angle-down"></i>
		</a>
	</li>
@endsection
@section('header')
	<!--begin::Page Vendors Styles -->
	{!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
	<!--end::Page Vendors Styles -->
@endsection

@section('content')
	<!-- END: Subheader -->
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">التحكم بالطلبات
						</h3>
					</div>
				</div>
			</div>
		</div>
			<div class="m-portlet__body">


<br><br> @include('error.error')
			@if($orders->count())
				@include('flash::message')
				<!--begin: Datatable -->
				<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_users">
					<thead>
						<tr>
							<th>#</th>
							<th>الاسم  </th>
							<th>العدد  </th>
                            <th>الحاله</th>
							<th>السعر</th>
							<th>الوقت</th>
                            <th>تفاصيل </th>
							<th>حذف</th>
						</tr>
					</thead>
					<tbody>
						@foreach($orders as $order)
						<tr class="debTr">
                            <th>{{$order->id}}</th>
							<th>name</th>
							<th>{{$order->number}}</th>
							<th>{{$order->status}}</th>
							<th>{{$order->price}}</th>
                            <th>{{$order->created_at}}</th>


							<th>
								<a href="{{url('admincp/orders/'.$order->id.'/show')}}" class="btn btn-brand m-btn editDeps">تفاصيل</a>

							</th>
							<th>
								<div class="form-group">


									{!! Form::open([

                                  'action'=>['Admin\OrderController@destroy',$order->id],
                                  'method'=>'delete'
                              ]) !!}

									<button class="btn btn-danger">حذف</button>

									{!! Form::close() !!}


								</div>

							</th>
						</tr>
						@endforeach
					</tbody>
				</table>

			</div>

		<!-- END EXAMPLE TABLE PORTLET-->
	</div>

	@else

		<div class="alert alert-danger">
			no data
		</div>
	@endif
</div>
	</div>
	</div>
@endsection
@section('footer')
	<!--begin::Page Vendors -->
	{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}
	{!! Html::script('admin/custom/js/countries.js') !!}
	<!--end::Page Vendors -->
<script type="text/javascript">
	var table = $('#m_table_users');
	// begin first table
	table.DataTable({
      	language: {
      	    aria: {
      	        sortAscending: ": ترتيب تصاعدى",
      	        sortDescending: ": ترتيب تنازلى"
      	    },
      	    emptyTable: "لا توجد اى بيانات متاحه",
      	    info: "إظهار _START_ إلى _END_ من _TOTAL_ حقل",
      	    infoEmpty: "لا توجد حقول",
      	    infoFiltered: "( الإجمالى _MAX_ حقل )",
      	    lengthMenu: "عدد الحقول : _MENU_",
      	    search: " بحث بإسم المستخدم :",
      	    zeroRecords: "لا توجد نتائج "
      	},

		responsive: true,
		order: [[0, "desc"]],
        lengthMenu: [[10, 20, 30, 50, -1], [10, 20, 30, 50, "الكل"]],
        pageLength: 10,
      	columnDefs: [{ "targets": [0,2,3,4], "searchable": false },{ "targets": [4], "orderable": false }]
	});


	$(document).on("click",".delUser",function(e) {
		e.preventDefault();
		var url = $(this).attr("href"),
		userId = $(this).data('id'),
		pData = {
			_token : "{{csrf_token()}}",
			_method : "delete"
		},
		ths = $(this);
		$.post(url,pData,function(data){
			toastrNotifyResponse(data);
			if(data.success){
				ths.closest('tr').remove();
			}
		});
	});
</script>
@endsection

    </div>