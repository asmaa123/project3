@extends('Admin.layouts.app')
@inject('model','App\Models\Color')
@section('title')
اضافه لون
@endsection
@section('topBar')
	<li class="m-menu__item">
		<a href="{{url('admincp')}}" class="m-menu__link">
			<span class="m-menu__link-text">الرئيسية</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item">
		<a href="{{url('admincp/users')}}" class="m-menu__link">
			<span class="m-menu__link-text">التحكم بالالوان </span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item" style="background: #e0deff;">
		<a href="javascript:;" class="m-menu__link">
			<span class="m-menu__link-text"></span>
			<i class="m-menu__hor-arrow la la-angle-down"></i>
		</a>
	</li>
@endsection

@section('header')
@endsection

@section('content')
	<!--begin::Portlet-->
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text">

					</h3>
				</div>
			</div>
		</div>

	{!!Form::model($model,[
'action'=>'Admin\ColorController@store',
'files'=>true,
'class'=>'m-form m-form--fit m-form--label-align-right'
]) !!}



			<div class="col-lg-12">
			<label class=" col-form-label">   اسم الالوان  عربي</label>

			<div class="col-lg-9">


			{!!Form::text('color[ar]',null,[
            'class'=>'form-control m-input','placeholder'=>" اسم الالوان  عربي"
            ])!!}
			</div>
			</div>


		<div class="col-lg-12">
			<label class="col-form-label">   اسم الالوان  انجليزي</label>

			<div class="col-lg-9">


				{!!Form::text('color[en]',null,[
                'class'=>'form-control m-input','placeholder'=>" اسم الالوان  انجليزي"
                ])!!}
			</div>
		</div>





<br><br>
			<br>
			<div class='form-group' >
				<br>
				<button class="btn btn-primary" type="submit"> submit</button>
			</div>

		</div>




		{!! Form::close()!!}
	<!--end::Form-->
	</div>
	<!--end::Portlet-->
@endsection
@section('footer')
<script type="text/javascript">

</script>
@endsection

