@extends('Admin.layouts.app')

@section('title')
	المنتجات
@endsection
@section('topBar')
	<li class="m-menu__item">
		<a href="{{url('admincp')}}" class="m-menu__link">
			<span class="m-menu__link-text">الرئيسية</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item" style="background: #e0deff;">
		<a href="{{url('admincp/products')}}" class="m-menu__link">
			<span class="m-menu__link-text">التحكم بالمنتجات</span>
			<i class="m-menu__hor-arrow la la-angle-down"></i>
		</a>
	</li>
@endsection
@section('header')
	<!--begin::Page Vendors Styles -->
	{!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
	<!--end::Page Vendors Styles -->
@endsection

@section('content')
	<!-- END: Subheader -->
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">التحكم بالمنتجات
						</h3>
					</div>
				</div>
			</div>
		</div>


			<div class="m-portlet__body">
				<a href="{{url(route('products.create'))}}" class="btn btn_primary btn btn-danger" ><i class=" fa fa-edit"></i>اضافه جديد</a>
				<br><br> @include('error.error')
				@if($products->count())
					@include('flash::message')

					<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_users">
						<thead>
						<tr>
							<th>#</th>

							<th>اسم المنتج  </th>

							<th>اسم الصنف </th>



							<th>اسم المتجر </th>


							<th> العرض</th>


							<th> السعر</th>

                            <th>النوع</th>

							<th>التخفيض</th>

							<th>الوقت</th>

							<th>الصوره الرئيسية</th>

							<th>تعديل </th>

							<th>حذف</th>

						</tr>
						</thead>
						<tbody>
						@foreach($products as $product)

							<tr class="debTr">

								<th>{{$product->id}}</th>
								<th>{{getJsonData($product->name,'ar')}}</th>
								<th>{{isset($product->catogery->name)?getJsonData($product->catogery->name,'ar'):''}}</th>
								<th>{{isset($product->stores()->first()->name)?$product->stores()->first()->name:''}}</th>
								<th>{{isset($product->offer->title)?$product->offer()->title:''}}</th>
								<th>{{$product->price}}</th>
								<th>{{$product->type}}</th>
								<th>{{$product->discount_value}}</th>
								<th>{{$product->created_at}}</th>


								@php

									$pro=App\Models\Image::where('imageable_type',0)->where('imageable_id',$product->id)->first();
								@endphp
								<th> <img src="{{isset($pro->image)?asset('uploads/' . $pro->image):''}}" style="width:200px; height:100px"></th>


								<th>
									<a href="{{url('admincp/products/'.$product->id.'/edit')}}" class="btn btn-brand m-btn editDeps">تعديل</a>

								</th>
								<th>
									<div class="form-group">


										{!! Form::open([

                                      'action'=>['Admin\ProductController@destroy',$product->id],
                                      'method'=>'delete'
                                  ]) !!}

										<button class="btn btn-danger">حذف</button>

										{!! Form::close() !!}
@endforeach

									</div>

								</th>

							</tr>
						</tbody>
					</table>

				@else

					<div class="alert alert-danger">
						no data
					</div>
				@endif






		<div >

</div>


		</div>

	<!-- END EXAMPLE TABLE PORTLET-->
	</div>

		</div>

</div></div>
	</div>

@endsection
@section('footer')
	<!--begin::Page Vendors -->
	{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}
	{!! Html::script('admin/custom/js/countries.js') !!}
	<!--end::Page Vendors -->
<script type="text/javascript">
	var table = $('#m_table_users');
	// begin first table
	table.DataTable({
      	language: {
      	    aria: {
      	        sortAscending: ": ترتيب تصاعدى",
      	        sortDescending: ": ترتيب تنازلى"
      	    },
      	    emptyTable: "لا توجد اى بيانات متاحه",
      	    info: "إظهار _START_ إلى _END_ من _TOTAL_ حقل",
      	    infoEmpty: "لا توجد حقول",
      	    infoFiltered: "( الإجمالى _MAX_ حقل )",
      	    lengthMenu: "عدد الحقول : _MENU_",
      	    search: " بحث بإسم المستخدم :",
      	    zeroRecords: "لا توجد نتائج "
      	},

		responsive: true,
		order: [[0, "desc"]],
        lengthMenu: [[10, 20, 30, 50, -1], [10, 20, 30, 50, "الكل"]],
        pageLength: 10,
      	columnDefs: [{ "targets": [0,2,3,4], "searchable": false },{ "targets": [4], "orderable": false }]
	});


	$(document).on("click",".delUser",function(e) {
		e.preventDefault();
		var url = $(this).attr("href"),
		userId = $(this).data('id'),
		pData = {
			_token : "{{csrf_token()}}",
			_method : "delete"
		},
		ths = $(this);
		$.post(url,pData,function(data){
			toastrNotifyResponse(data);
			if(data.success){
				ths.closest('tr').remove();
			}
		});
	});
</script>
@endsection
	</div>
