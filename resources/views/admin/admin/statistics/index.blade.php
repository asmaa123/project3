@extends('Admin.layouts.app')
@inject('user','App\Models\User')
@inject('catogery','App\Models\Catogery')
@inject('product','App\Models\Product')

@section('title')

    عرض الاحصائيات
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('admincp')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item" style="background: #e0deff;">
        <a href="javascript:;" class="m-menu__link">
            <span class="m-menu__link-text">التحكم بالاحصائيات</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>
@endsection
@section('header')
    <!--begin::Page Vendors Styles -->
    {!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
    <!--end::Page Vendors Styles -->
@endsection

@section('content')
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head" style="height: auto;background: #e0deff">
                <div class="col-sm-6 col-md-4 col-xs-12">


                    <div class="info-box">


                        <span class="info-box-icon by-aqua"><i class="fa fa-user"></i></span>

                        <div class="info-box-content">

				<span class="info-box-text">


					<span class="info-box-number">

					المستخدمين
						{{$user->count()}}


					</span>
				</span>



                        </div>


                    </div>


                </div>
                <div class="col-sm-6 col-md-4 col-xs-12">


                    <div class="info-box">


                        <span class="info-box-icon by-aqua"><i class="fa fa-tag"></i></span>

                        <div class="info-box-content">

				<span class="info-box-text">


					<span class="info-box-number">
						 الاقسام {{$catogery->count()}}


					</span>
				</span>



                        </div>


                    </div>


                </div>
                <div class="col-sm-6 col-md-4 col-xs-12">


                    <div class="info-box">


                        <span class="info-box-icon by-aqua"><i class="fa fa-file"></i></span>

                        <div class="info-box-content">

				<span class="info-box-text">


					<span class="info-box-number">

					المنتجات{{$product->count()}}


					</span>
				</span>



                        </div>


                    </div>


                </div>

                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">

                    </div>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">


        </div>

    </div>

    </div></div>
    </div>





@endsection
@section('footer')
    <!--begin::Page Vendors -->
    {!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}
    {!! Html::script('admin/custom/js/countries.js') !!}
    <!--end::Page Vendors -->
    <script type="text/javascript">
        var table = $('#m_table_users');
        // begin first table
        table.DataTable({
            language: {
                aria: {
                    sortAscending: ": ترتيب تصاعدى",
                    sortDescending: ": ترتيب تنازلى"
                },
                emptyTable: "لا توجد اى بيانات متاحه",
                info: "إظهار _START_ إلى _END_ من _TOTAL_ حقل",
                infoEmpty: "لا توجد حقول",
                infoFiltered: "( الإجمالى _MAX_ حقل )",
                lengthMenu: "عدد الحقول : _MENU_",
                search: " بحث بإسم المستخدم :",
                zeroRecords: "لا توجد نتائج "
            },

            responsive: true,
            order: [[0, "desc"]],
            lengthMenu: [[10, 20, 30, 50, -1], [10, 20, 30, 50, "الكل"]],
            pageLength: 10,
            columnDefs: [{ "targets": [0,2,3,4], "searchable": false },{ "targets": [4], "orderable": false }]
        });


        $(document).on("click",".delUser",function(e) {
            e.preventDefault();
            var url = $(this).attr("href"),
                userId = $(this).data('id'),
                pData = {
                    _token : "{{csrf_token()}}",
                    _method : "delete"
                },
                ths = $(this);
            $.post(url,pData,function(data){
                toastrNotifyResponse(data);
                if(data.success){
                    ths.closest('tr').remove();
                }
            });
        });
    </script>
    @endsection
    </div>
