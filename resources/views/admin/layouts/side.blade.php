<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/statistics')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-line-graph"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">إحصائيات عامة</span>
            </span>
        </span>
    </a>
</li>






<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
    <a href="javascript:;" class="m-menu__link m-menu__toggle">
        <i class="m-menu__link-icon flaticon-layers"></i>
        <span class="m-menu__link-text">إعدادات الموقع</span>
        <i class="m-menu__ver-arrow la la-angle-right"></i>
    </a>
    <div class="m-menu__submenu ">
        <span class="m-menu__arrow"></span>
        <ul class="m-menu__subnav">
            <li class="m-menu__item " aria-haspopup="true">
                <a href="{{url('admincp/settings')}}" class="m-menu__link ">
                    <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                        <span></span>
                    </i>
                    <span class="m-menu__link-text">الإعدادت العامه</span>
                </a>
            </li>
        </ul>
    </div>
</li>











<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/catogeries')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-grid-menu-v2" ></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاقسام </span>
            </span>
        </span>
    </a>
</li>








<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/orders')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-open-box" ></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الطلبات </span>
            </span>
        </span>
    </a>
</li>










<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/platforms')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-layers" ></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">المنتصات</span>
            </span>
        </span>
    </a>
</li>




<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/user')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-user "></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">المستخدمين </span>
            </span>
        </span>
    </a>
</li>




<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/admins')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-user "></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الادمن </span>
            </span>
        </span>
    </a>
</li>



<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/settings')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-settings "></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاعدادت </span>
            </span>
        </span>
    </a>
</li>








<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/colors')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-edit"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الالوان  </span>
            </span>
        </span>
    </a>
</li>






<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/images')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-photo-camera"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الصور  </span>
            </span>
        </span>
    </a>
</li>



<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/contacts')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-mail-1"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">تواصل معنا  </span>
            <span class="kt-menu__link-badge"><span class="kt-badge kt-badge--rounded kt-badge--brand btn-danger fa fa-2x">

                @php  $cout=App\Models\Contact::where('read_at',0)->count()

                @endphp
                    {{$cout}}

                </span></span>
            </span>
        </span>
    </a>
</li>


<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/sliders')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-folder-1" ></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">السليدر</span>
            </span>
        </span>
    </a>
</li>



<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/advertisements')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-folder-1" ></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاعلانات</span>
            </span>
        </span>
    </a>
</li>





<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/pages')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-file" ></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الصفحات الثابته</span>
            </span>
        </span>
    </a>
</li>








<div class="m-menu__submenu ">
    <span class="m-menu__arrow"></span>
    <ul class="m-menu__subnav">
        <li class="m-menu__item " aria-haspopup="true">
            <a href="#" class="m-menu__link ">
                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                    <span></span>
                </i>
                <span class="m-menu__link-text">قائمة أولى</span>
            </a>
        </li>


        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                <span class="m-menu__link-text">قائمة فرعيه</span>
                <i class="m-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="#" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">قائمة</span>
                        </a>
                    </li>



                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="#" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">قائمة</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="m-menu__item" aria-haspopup="true">
            <a href="{{url('admincp/users')}}" class="m-menu__link ">
                <i class="m-menu__link-icon flaticon-file"></i>
                <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">user</span>
            </span>
        </span>
            </a>
        </li>



    </ul>
</div>
</li>
