@extends('Admin.layouts.app')

@section('title')
	عرض المنتصات
@endsection
@section('topBar')
	<li class="m-menu__item">
		<a href="{{url('admincp')}}" class="m-menu__link">
			<span class="m-menu__link-text">الرئيسية</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item" style="background: #e0deff;">
		<a href="{{url('admincp\platforms')}}" class="m-menu__link">
			<span class="m-menu__link-text">التحكم بالمنتصات</span>
			<i class="m-menu__hor-arrow la la-angle-down"></i>
		</a>
	</li>
@endsection
@section('header')
	<!--begin::Page Vendors Styles -->
	{!! Html::style('admin/vendors/custom/datatables/datatables.bundle.rtl.css') !!}
	<!--end::Page Vendors Styles -->
@endsection

@section('content')
	<!-- END: Subheader -->
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">التحكم بالمنتصات
						</h3>
					</div>
				</div>
			</div>
		</div>
			<div class="m-portlet__body">

				<a href="{{url(route('platforms.create'))}}" class="btn btn_primary btn btn-danger" ><i class=" fa fa-edit"></i>اضافه جديد</a>
<br><br> @include('error.error')
			@if($platforms->count())
				@include('flash::message')
				<!--begin: Datatable -->
				<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_users">
					<thead>
						<tr>
							<th>#</th>
							<th>الاسم  </th>
							<th> النص</th>

                             <th>الوقت</th>
							<th>الصوره الرئيسية</th>
                            <th>تعديل </th>
							<th>حذف</th>
						</tr>
					</thead>
					<tbody>
						@foreach($platforms as $platform)
						<tr class="debTr">
                            <th>{{$platform->id}}</th>
							<th>{{getJsonData($platform->name,'ar')}}</th>
							<th>{{getJsonData($platform->text,'ar')}}</th>
                            <th>{{$platform->created_at}}</th>

							@php

								$pro=App\Models\Image::where('imageable_type',0)->where('imageable_id',$platform->id)->first();
							@endphp
							<th> <img src="{{isset($pro->image)?asset('uploads/' . $pro->image):''}}" style="width:200px; height:100px"></th>

							<th>
								<a href="{{url('admincp/platforms/'.$platform->id.'/edit')}}" class="btn btn-brand m-btn editDeps">تعديل</a>

							</th>
							<th>
								<div class="form-group">


									{!! Form::open([

                                  'action'=>['Admin\PlatformController@destroy',$platform->id],
                                  'method'=>'delete'
                              ]) !!}

									<button class="btn btn-danger">حذف</button>

									{!! Form::close() !!}


								</div>

							</th>
						</tr>
						@endforeach
					</tbody>
				</table>

			</div>

		<!-- END EXAMPLE TABLE PORTLET-->
	</div>

	@else

		<div class="alert alert-danger">
			no data
		</div>
	@endif
</div>
	</div>
	</div>
@endsection
@section('footer')
	<!--begin::Page Vendors -->
	{!! Html::script('admin/vendors/custom/datatables/datatables.bundle.js') !!}
	{!! Html::script('admin/custom/js/countries.js') !!}
	<!--end::Page Vendors -->
<script type="text/javascript">
	var table = $('#m_table_users');
	// begin first table
	table.DataTable({
      	language: {
      	    aria: {
      	        sortAscending: ": ترتيب تصاعدى",
      	        sortDescending: ": ترتيب تنازلى"
      	    },
      	    emptyTable: "لا توجد اى بيانات متاحه",
      	    info: "إظهار _START_ إلى _END_ من _TOTAL_ حقل",
      	    infoEmpty: "لا توجد حقول",
      	    infoFiltered: "( الإجمالى _MAX_ حقل )",
      	    lengthMenu: "عدد الحقول : _MENU_",
      	    search: " بحث بإسم المستخدم :",
      	    zeroRecords: "لا توجد نتائج "
      	},

		responsive: true,
		order: [[0, "desc"]],
        lengthMenu: [[10, 20, 30, 50, -1], [10, 20, 30, 50, "الكل"]],
        pageLength: 10,
      	columnDefs: [{ "targets": [0,2,3,4], "searchable": false },{ "targets": [4], "orderable": false }]
	});


	$(document).on("click",".delUser",function(e) {
		e.preventDefault();
		var url = $(this).attr("href"),
		userId = $(this).data('id'),
		pData = {
			_token : "{{csrf_token()}}",
			_method : "delete"
		},
		ths = $(this);
		$.post(url,pData,function(data){
			toastrNotifyResponse(data);
			if(data.success){
				ths.closest('tr').remove();
			}
		});
	});
</script>
@endsection

    </div>