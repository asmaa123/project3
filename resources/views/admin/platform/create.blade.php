@extends('Admin.layouts.app')
@inject('model','App\Models\Platform')
@section('title')
	اضافه المنصه
@endsection
@section('topBar')
	<li class="m-menu__item">
		<a href="{{url('admincp')}}" class="m-menu__link">
			<span class="m-menu__link-text">الرئيسية</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item">
		<a href="{{url('admincp/platforms')}}" class="m-menu__link">
			<span class="m-menu__link-text">التحكم بالمنتصات</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item" style="background: #e0deff;">
		<a href="javascript:;" class="m-menu__link">
			<span class="m-menu__link-text"></span>
			<i class="m-menu__hor-arrow la la-angle-down"></i>
		</a>
	</li>
@endsection

@section('header')
@endsection

@section('content')
	<!--begin::Portlet-->
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text">
						اضافه المنصه
					</h3>
				</div>
			</div>
		</div>

		{!!Form::model($model,[
    'action'=>'Admin\PlatformController@store',
    'files'=>true,
    'class'=>'m-form m-form--fit m-form--label-align-right'
    ]) !!}



		<div class="col-lg-12">
			<label class=" col-form-label">   اسم  عربي</label>

			<div class="col-lg-9">


				{!!Form::text('name[ar]',null,[
                'class'=>'form-control m-input','placeholder'=>" اسم  عربي"
                ])!!}
			</div>
		</div>


		<div class="col-lg-12">
			<label class="col-form-label">   اسم  انجليزي</label>

			<div class="col-lg-9">


				{!!Form::text('name[en]',null,[
                'class'=>'form-control m-input','placeholder'=>" اسم  انجليزي"
                ])!!}
			</div>
		</div>



		<div class="col-lg-12">
			<label class="col-form-label">   النص عربي</label>

			<div class="col-lg-9">


				{!!Form::text('text[ar]',null,[
                'class'=>'form-control m-input','placeholder'=>" النص انجليزي"
                ])!!}
			</div>
		</div>



		<div class="col-lg-12">
			<label class="col-form-label">   النص انجليزي</label>

			<div class="col-lg-9">


				{!!Form::text('text[en]',null,[
                'class'=>'form-control m-input','placeholder'=>" النص  انجليزي"
                ])!!}
			</div>
		</div>




		<div class="col-lg-12">
			<label class="col-form-label">   الصوره الفرعيه</label>

			<div class="col-lg-9">

				<input type="file" id="file" multiple="multiple" class="file-input form-control" accept="image/*"
					   name="images[]">


			</div>
		</div>


		<div class="col-lg-12">
			<label class="col-form-label">   الصوره الرئيسية</label>

			<div class="col-lg-9">

				<input type="file" id="file" multiple="multiple" class="file-input form-control" accept="image/*"
					   name="image">


			</div>
		</div>

		<br><br>
		<br>
		<div class='form-group' >
			<br>
			<button class="btn btn-primary" type="submit"> submit</button>
		</div>

	</div>




	{!! Form::close()!!}
	<!--end::Form-->
	</div>
	<!--end::Portlet-->
@endsection
@section('footer')
	<script type="text/javascript">

	</script>
@endsection

