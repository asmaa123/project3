<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model 
{

    protected $table = 'users';
    public $timestamps = true;
    protected $fillable = array('email', 'name');

    public function contacts()
    {
        return $this->hasMany('App\Models\Contact');
    }

    public function messages()
    {
        return $this->hasMany('App\Models\Message');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product');
    }

    public function platforms()
    {
        return $this->belongsToMany('App\Models\Platform');
    }

    public function profile()
    {
        return $this->hasOne('App\Models\Profile','user_id');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function ratings()
    {
        return $this->morphMany('App\Models\Rating', 'rateable');
    }

    public function stores()
    {
        return $this->hasMany('App\Models\User');
    }

    public function carts()
    {
        return $this->hasMany('App\Models\Cart');
    }

    public function platform()
    {
        return $this->hasMany('App\Models\Platform');
    }

}