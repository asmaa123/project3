<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model 
{

    protected $table = 'products';
    public $timestamps = true;
    protected $fillable = array('type', 'price', 'details', 'name', 'discount_value','catogery_id');



    public function catogery()
    {
        return $this->belongsTo(Catogery::class,'catogery_id','id');
    }

    public function offer()
    {
        return $this->hasMany(Offer::class);
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class,'product_order','product_id','order_id');
    }

    public function images()
    {
        return $this->morphMany('App\Models\Image','imageable');
    }

    public function colors()
    {
        return $this->hasMany('App\Models\Color');
    }

    public function carts()
    {
        return $this->hasMany('App\Models\Cart');
    }

    public function ratings()
    {
        return $this->morphMany('App\Models\Rating', 'rateable');
    }

    public function stores()
    {
        return $this->belongsToMany(Store::class,'product_store','product_id','store_id');
    }

}