<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model 
{

    protected $table = 'stores';
    public $timestamps = true;
    protected $fillable = array('user_id', 'logo', 'lng', 'lat');

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function catogery()
    {
        return $this->belongsTo('App\Models\Catogery');
    }

    public function ratings()
    {
        return $this->morphMany('App\Models\User', 'rateable');
    }

    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }

    public function carts()
    {
        return $this->hasMany('App\Models\Cart');
    }

    public function messages()
    {
        return $this->hasMany('App\Models\Message');
    }

    public function platforms()
    {
        return $this->hasMany('App\Models\Platform');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class,'product_store','product_id','store_id');
    }

}