<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_user extends Model 
{

    protected $table = 'product_user';
    public $timestamps = true;
    protected $fillable = array('product_id', 'user_id');

}