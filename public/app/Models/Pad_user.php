<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pad_user extends Model 
{

    protected $table = 'pad_user';
    public $timestamps = true;
    protected $fillable = array('pad_id', 'user_id');

}