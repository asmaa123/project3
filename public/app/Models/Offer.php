<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model 
{

    protected $table = 'offers';
    public $timestamps = true;
    protected $fillable = array('image', 'product_id','category_id');

    public function products()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }

    public function category()
    {
        return $this->belongsTo(Catogery::class,'category_id','id');
    }

}