<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_order extends Model 
{

    protected $table = 'product_order';
    public $timestamps = true;
    protected $fillable = array('product_id', 'order_id');

}