<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model 
{

    protected $table = 'images';
    public $timestamps = true;
    protected $fillable = array('imageable_id', 'imageable_type', 'image');

    public function product()
    {
        return $this->morphTo();
    }

    public function store()
    {
        return $this->morphTo();
    }

    public function platforms()
    {
        return $this->morphMany('App\Models\Platform', 'imageable');
    }

}