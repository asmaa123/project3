<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Catogery extends Model 
{

    protected $table = 'catogeries';
    public $timestamps = true;
    protected $fillable = array('name', 'image');

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function stores()
    {
        return $this->hasMany('App\Models\User');
    }

    public function platforms()
    {
        return $this->hasMany('App\Models\Platform');
    }

}