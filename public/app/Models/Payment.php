<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model 
{

    protected $table = 'payments';
    public $timestamps = true;
    protected $fillable = array('fullname', 'number', 'type', 'price', 'total', 'charges', 'title');
    protected $visible = array('dicount');

}