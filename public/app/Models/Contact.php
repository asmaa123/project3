<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model 
{

    protected $table = 'contacts';
    public $timestamps = true;
    protected $fillable = array('name', 'email', 'content');

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

}