<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductdResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,

            'weight' => $this->weight,

            'name' => getApiJsonData($this->name,request()->header('accept-language')),

            'protine' => $this->protine,

            'calories' => $this->calories,

            'fiber' => $this->fiber,

            'carbonydrate' => $this->carbonydrate,

            'created_at' => $this->created_at,

            'updated_at' => $this->updated_at



        ];
    }
}
