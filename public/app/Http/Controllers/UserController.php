<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\User;
use App\Department;
use DB;
use Hash;
use Validator;

class UserController extends Controller
{

    function __construct()
    {
         $this->middleware('permission:user-list', ['only' => ['index','show']]);
         $this->middleware('permission:user-create', ['only' => ['create','store']]);
         $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('admin.users.index',compact('users'));
    }

    public function show ($type){
        switch ($type) {
            case 'archive-users': 
                $users = User::onlyTrashed()->get(); 
            break;
            default:
                return back()->withFlashMessage(json_encode(['success'=>false,'msg'=>'عذرا لقد طلبت صفحة غير موجوده']));
                break;
        }
        return view('admin.users.index',compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('admin.users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $data = $request->validate([
            'name'              => 'required|max:255',
            "email"             => 'required|email|max:255|unique:users,email,' .$user->id,
            "phone"             => 'required|numeric|unique:users,phone,' .$user->id,
        ],[
            'name.required'     => 'الإسم مطلوب',
            "email.required"    => 'البردي الإبكترونى مطلوب',
            "phone.required"    => 'رقم الهاتف مطلوب',
        ]);

        $password = $request->validate([
            'password'          => request('password') ? 'min:6|confirmed' : '',
        ]);

        if(request('curPassword')){
            if(Hash::check(request('curPassword'), $user->password)){
                $user->update(['password' => bcrypt(request('password'))]);                
            }else{
                return back()->withErrors(['curPassword'=>'كلمة المرور القديمة غير صحيحه']);
            }
        }

        $user->update($data);
        return back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل البيانات بنجاح']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return response()->json(['success'=>true,'msg'=>'تم نقل المستخدم بنجاح']);
    }

    public function restore($id){
        if(Auth()->user()->id == $id){
            return back()->withFlashMessage(json_encode(['success'=>false,'msg'=>'عذرا لا يمكنك حذف الحساب الخاص بك']));
        }
        $user = User::where('id',$id)->first();
        $user->restore();
        return back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم إسترجاع المستخدم بنجاح']));
    }

    public function force($id){
        if(Auth()->user()->id == $id){
            return back()->withFlashMessage(json_encode(['success'=>false,'msg'=>'عذرا لا يمكنك حذف الحساب الخاص بك']));
        }
        
        $delUser = User::where('id',$id)->withTrashed()->get()->first();
        $delUser->forceDelete();
        return back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم حذف المستخدم بشكل نهائى']));
    }

    public function changePassword (Request $request)
    {
        if(request()->isMethod('get')){
            return view('admin.users.changePassword');
        }else{
            $data = $request->validate([
                'password'              => 'required|min:6|confirmed',
            ],[
                'password.required'     => 'أكتب كلمة المرور',
            ]);
            $user = \Auth::user();
            if(request('curPassword')){
                if(Hash::check(request('curPassword'), $user->password)){
                    $user->update(['password' => bcrypt(request('password'))]);                
                }else{
                    return back()->withErrors(['curPassword'=>'كلمة المرور القديمة غير صحيحه']);
                }
            }
            return back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم تعديل كلمة المرور بنجاح']));
        }
    }
}
