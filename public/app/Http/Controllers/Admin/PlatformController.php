<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Platform;


use App\Models\Image;

class PlatformController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $platforms=Platform::all();

       return view('admin.platform.index',compact('platforms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.platform.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            'name' => 'required|max:191',
            'image' => 'required',
            'text' => 'required'

        ];

        $message = [

            'name.requied' => 'this is required',
            'image.requied' => 'this is required',
            'text.requied' => 'this is required'

        ];

        $this->validate($request, $rule, $message);

        $platform = Platform::create([
            'name' => json_encode($request->name, JSON_UNESCAPED_UNICODE),
            'text' => json_encode($request->text, JSON_UNESCAPED_UNICODE)

        ]);

        $name = $request->file('image');
        $fileName = "";
//        $img = str_random(4) . $name->getClientOriginalName();
        $fileName = 'main_pro-' . time() . '-' . uniqid() . '.' . $name->getClientOriginalExtension();
        $destinationPath = 'uploads';
        $name->move($destinationPath, $fileName);
        $image = new Image();
        $image->image = $fileName;
        $image->imageable_id = $platform->id;
        $image->imageable_type = 0;
        $image->save();
        $imageNames = $request->file('images');
        foreach ((array)$imageNames as $name) {
            $fileName = "";
//            $img = str_random(4) . $name->getClientOriginalName();
            $fileName = 'main_pro-' . time() . '-' . uniqid() . '.' . $name->getClientOriginalExtension();
            $destinationPath = 'uploads';
            $name->move($destinationPath, $fileName);
            $image = new Image();
            $image->image = $fileName;
            $image->imageable_id = $platform->id;
            $image->imageable_type = 1;
            $image->save();
        }

        flash()->message('تم إضافة المنصه بنجاح');

        return redirect(route('platforms.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $platform=Platform::find($id);

        return view('admin.platform.edit',compact('platform'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $platform=Platform::find($id);

        $platform->update([
            'name'=> json_encode($request->name, JSON_UNESCAPED_UNICODE),
            'text'=> json_encode($request->text, JSON_UNESCAPED_UNICODE)
        ]);

        $name = $request->file('image');

        if ($request->has('image')) {
            $fileName = 'main_pro-' . time() . '-' . uniqid() . '.' . $name->getClientOriginalExtension();
            $destinationPath = 'uploads';
            $name->move($destinationPath, $fileName);
            $image = Image::where('imageable_id', $platform->id)->where('imageable_type', 0)->first();
            $image->image = $fileName;
            $image->save();
        }

        $imageNames = $request->file('images');
        foreach ((array)$imageNames as $name) {
            $fileName = "";
//           $img = str_random(4) . $name->getClientOriginalName();
            $fileName = 'main_pro-' . time() . '-' . uniqid() . '.' . $name->getClientOriginalExtension();
            $destinationPath = 'uploads';
            $name->move($destinationPath, $fileName);
            $image = new Image();
            $image->image = $fileName;
            $image->imageable_id = $platform->id;
            $image->imageable_type = 1;
            $image->save();
        }


        flash()->message('تم تعديل المنصه بنجاح');

        return redirect(route('platforms.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $platform=Platform::find($id);
        $platform->delete();
        flash()->message('تم حذف المنصه بنجاح');
        return back();
    }
}
