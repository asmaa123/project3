<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Product;

use App\Models\Image;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $products = Product::with(['catogery', 'users','stores','offer'])->get();

        return view('admin.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            'name' => 'required|max:191',
            'type' => 'required',
            'price' => 'required',
            'details' => 'required',
            'discount_value' => 'required'

        ];

        $message = [

            'name.requied' => 'this is required',
            'type.requied' => 'this is required',
            'price.requied' => 'this is required',
            'details.requied' => 'this is required',
            'discount_value.requied' => 'this is required'

        ];

        $this->validate($request, $rule, $message);


        $product = Product::create([
            'name' => json_encode($request->name, JSON_UNESCAPED_UNICODE),
            'details' => json_encode($request->details, JSON_UNESCAPED_UNICODE),
            'title' => json_encode($request->title, JSON_UNESCAPED_UNICODE),
            'type' => $request->type,
            'price' => $request->price,
            'catogery_id' => $request->catogery_id,
            'discount_value' => $request->discount_value

        ]);
        $name = $request->file('image');
        $fileName = "";
//        $img = str_random(4) . $name->getClientOriginalName();
        $fileName = 'main_pro-' . time() . '-' . uniqid() . '.' . $name->getClientOriginalExtension();
        $destinationPath = 'uploads';
        $name->move($destinationPath, $fileName);
        $image = new Image();
        $image->image = $fileName;
        $image->imageable_id = $product->id;
        $image->imageable_type = 0;
        $image->save();
        $imageNames = $request->file('images');
        foreach ($imageNames as $name) {
            $fileName = "";
//            $img = str_random(4) . $name->getClientOriginalName();
            $fileName = 'main_pro-' . time() . '-' . uniqid() . '.' . $name->getClientOriginalExtension();
            $destinationPath = 'uploads';
            $name->move($destinationPath, $fileName);
            $image = new Image();
            $image->image = $fileName;
            $image->imageable_id = $product->id;
            $image->imageable_type = 1;
            $image->save();
        }

        flash()->message('تم إضافة المنتج بنجاح');

        return redirect(route('products.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);

        return view('admin.product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $product->update([
            'name' => json_encode($request->name, JSON_UNESCAPED_UNICODE),
            'details' => json_encode($request->details, JSON_UNESCAPED_UNICODE),
            'title' => json_encode($request->title, JSON_UNESCAPED_UNICODE),
            'type' => $request->type,
            'price' => $request->price,
            'catogery_id' => $request->catogery_id,
            'discount_value' => $request->discount_value
        ]);

        $name = $request->file('image');

        if ($request->has('image')) {
            $fileName = 'main_pro-' . time() . '-' . uniqid() . '.' . $name->getClientOriginalExtension();
            $destinationPath = 'uploads';
            $name->move($destinationPath, $fileName);
            $image = Image::where('imageable_id', $product->id)->where('imageable_type', 0)->first();
            $image->image = $fileName;
            $image->save();
        }
        
        $imageNames = $request->file('images');
        foreach ($imageNames as $name) {
            $fileName = "";
//           $img = str_random(4) . $name->getClientOriginalName();
            $fileName = 'main_pro-' . time() . '-' . uniqid() . '.' . $name->getClientOriginalExtension();
            $destinationPath = 'uploads';
            $name->move($destinationPath, $fileName);
            $image = new Image();
            $image->image = $fileName;
            $image->imageable_id = $product->id;
            $image->imageable_type = 1;
            $image->save();
        }

        flash()->message('تم تعديل المنتج بنجاح');

        return redirect(route('products.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        flash()->message('تم حذف المنتج بنجاح');
        return back();
    }
}
