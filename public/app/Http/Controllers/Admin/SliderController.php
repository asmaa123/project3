<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slider;
use Intervention\Image\Facades\Image;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $sliders=Slider::all();

       return view('admin.slider.index',compact('sliders'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule=[
            'title'=>'required|max:191',
            'image'=>'required',
            'text'=>'required'

               ];

        $message=[

            'title.requied'=>'this is required',
            'image.requied'=>'this is required',
            'text.requied'=>'this is required'
                ];

        $this->validate($request,$rule,$message);

        $slider=Slider::create([
            'title'=> json_encode($request->title, JSON_UNESCAPED_UNICODE),
            'text'=> json_encode($request->text, JSON_UNESCAPED_UNICODE)
                              ]);

        if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->resize(300, 300)->save( public_path('/uploads/' . $filename ) );
            $slider->image = $filename;
            $slider->save();
                                           };

        flash()->message('تم إضافة السليدر بنجاح');

        return redirect(route('sliders.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider=Slider::find($id);

        return view('admin.slider.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider=Slider::find($id);
        $slider->update([
            'title'=> json_encode($request->title, JSON_UNESCAPED_UNICODE),
            'text'=> json_encode($request->text, JSON_UNESCAPED_UNICODE),
                       ]);


        if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->resize(200, 200)->save( public_path('/uploads/' . $filename ) );
            $slider->image = $filename;
            $slider->save();
                                            };


        flash()->message('تم  تعديل  السليدر  بنجاح');

        return redirect(route('sliders.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider=Slider::find($id);
        $slider->delete();
        flash()->message('تم حذف  السليدر  بنجاح');
        return back();
    }
}
