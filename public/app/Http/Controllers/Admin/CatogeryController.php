<?php

namespace App\Http\Controllers\Admin;

use App\Models\Catogery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;


class CatogeryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cats=  Catogery::all();

        return view('admin.catogery.index',compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.catogery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule=[
            'name'=>'required|max:191',
            'image'=>'required|mimes:png'

        ];

        $message=[

            'name.requied'=>'this is required',
            'image.requied'=>'this is required'
               ];

        $this->validate($request,$rule,$message);

        $cat=Catogery::create([
            'name'=> json_encode($request->name, JSON_UNESCAPED_UNICODE)
        ]);

        if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->resize(300, 300)->save( public_path('/uploads/' . $filename ) );
            $cat->image = $filename;
            $cat->save();
                                            };

        flash()->message('تم إضافة القسم بنجاح');

        return redirect(route('catogeries.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat=Catogery::find($id);

        return view('admin.catogery.edit',compact('cat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cat=Catogery::find($id);
        $cat->update([
            'name'=> json_encode($request->name, JSON_UNESCAPED_UNICODE)
                    ]);


        if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->resize(200, 200)->save( public_path('/uploads/' . $filename ) );
            $cat->image = $filename;
            $cat->save();
                                         };


        flash()->message('تم تعديل القسم بنجاح');

        return redirect(route('catogeries.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat=Catogery::find($id);
        $cat->delete();
        flash()->message('تم حذف القسم بنجاح');
        return back();
    }
}
