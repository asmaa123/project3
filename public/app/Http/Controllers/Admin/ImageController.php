<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Image;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images=Image::all();

        return view('admin.image.index',compact('images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.image.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule=[
            'image'=>'required',

             ];

        $message=[

            'image.requied'=>'this is required'

                  ];

        $this->validate($request,$rule,$message);




        $image=Image::create($request->all());


        if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            \Intervention\Image\Facades\Image::make($thumbnail)->resize(200, 200)->save( public_path('/uploads/' . $filename ) );
            $image->image = $filename;
            $image->save();
        };


             $image->save();




        flash()->message('تم إضافة الصوره بنجاح');

        return redirect(route('images.index'));




    }








    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $image=Image::find($id);

        return view('admin.image.edit',compact('image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $image=Image::find($id);

        $image->update($request->all());

        if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            \Intervention\Image\Facades\Image::make($thumbnail)->resize(200, 200)->save( public_path('/uploads/' . $filename ) );
            $image->image = $filename;
            $image->save();
                                          };


        $image->save();

        flash()->message('تم تعديل الصوره بنجاح');

        return redirect(route('images.index'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $image=Image::find($id);
       $image->delete();
        flash()->message('تم حذف الصوره بنجاح');
        return back();
    }
}
