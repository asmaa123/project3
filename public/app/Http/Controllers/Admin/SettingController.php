<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\SiteSetting;

class settingController extends Controller
{
   /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index(SiteSetting $setting){
    $settings = $setting->where('module','main')->orderBy('orderBy','asc')->get();
    return view('admin.settings.index',compact('settings'));
  }

  public function show($type){
    $customModuleArr = ['social','waterMark'];
    if(SiteSetting::where('module',$type)->count()){
      $settings = SiteSetting::where('module',$type)->orderBy('orderBy','asc')->get();
      $type = in_array($type, $customModuleArr) ? $type : 'index';
      return view('admin.settings.'.$type,compact('settings'));
    }else{
      Session()->flash('error_flash_message','طلبت صفحة غير موجوده');
      return back();
    }
  }

  public function store(Request $request,SiteSetting $sitesetting){
    $imagesOrExcetFields = ['_token','submit'];
    foreach ($request->files as $key => $value) {
      $imagesOrExcetFields[]= $key;
    }

    foreach ($imagesOrExcetFields as $img) {
      if($request->file($img) != null){
        SiteSetting::upSetImg($request->file($img),$img,$img);
      }
    }

    foreach(array_except($request->toArray(),$imagesOrExcetFields) as $key=>$reg){
      $settigsupdate = $sitesetting->where('name',$key)->first()->update(['value'=>$reg]);
    }
    image_upload($sitesetting,$request->file('waterMarkTest'),NULL,'/website','images');
    return redirect()->back()->withFlashMessage('تم تعديل اعدادات الموقع بنجاح');

  }

}
