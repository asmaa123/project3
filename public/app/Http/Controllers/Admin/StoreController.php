<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Store;
use App\Models\Image;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores=Store::with(['catogery','images'])->get();

        return view('admin.store.index',compact('stores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.store.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule=[
            'namee'=>'required|max:191',
            'image'=>'required',


        ];

        $message=[

            'namee.requied'=>'this is required',
            'image.requied'=>'this is required'

        ];

        $this->validate($request,$rule,$message);


        $store=Store::create([
            'namee'=>$request->namee,
            'catogery_id'=>$request->catogery_id,


                           ]);



        $name = $request->file('image');
        $fileName = "";
//        $img = str_random(4) . $name->getClientOriginalName();
        $fileName = 'main_pro-' . time() . '-' . uniqid() . '.' . $name->getClientOriginalExtension();
        $destinationPath = 'uploads';
        $name->move($destinationPath, $fileName);
        $image = new Image();
        $image->image = $fileName;
        $image->imageable_id =$store->id;
        $image->imageable_type = 0;
        $image->save();



        flash()->message('تم إضافة المتجر بنجاح');

        return redirect(route('stores.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $store=Store::find($id);

        return view('admin.store.edit',compact('store'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $store=Store::find($id);

        $store->update([
            'namee'=>$request->namee,
            'catogery_id'=>$request->catogery_id,

                      ]);

        flash()->message('تم تعديل المتجر بنجاح');

        return redirect(route('stores.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $store=Store::find($id);
        $store->delete();
        flash()->message('تم حدف المتجر بنجاح');
        return back();

    }
}
