<?php
namespace App\Http\Controllers\Api;
use Validator;

trait apiResponseTrait
{
	public $paginateNum = 10;

	public function apiResponse($result=[], $code=200, $header=[])
    {
        $response = $result;
        return response($response, $code, [], JSON_UNESCAPED_UNICODE)->withHeaders($header);
    }

    public function createdResponse($data)
    {
    	return $this->apiResponse($data,201);
    }

    public function updatedResponse($data)
    {
    	return $this->apiResponse($data,200);
    }

    public function deletedResponse()
    {
    	return $this->apiResponse("",204);
    }

    public function notFoundResponse()
    {
    	return $this->apiResponse("", 404);
    }
    public function dataError()
    {
        return $this->apiResponse("", 400);
    }
    public function dataNotSaved()
    {
        return $this->apiResponse("", 501);
    }
    
    public function apiValidation($r,$vldtArr)
    {
        $validator =Validator::make($r, $vldtArr);
        if($validator->fails()){
            $result=array("message"=>$validator->errors()->first());
            return $this->apiResponse($result,422);
        }
        return false;

    }

}
