<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_st_language')->insert([
            'lang_name_ar' => 'اللغة العربية',
            'lang_name_en' => 'Arabic',
        ]);
        DB::table('tbl_st_language')->insert([
            'lang_name_ar' => 'اللغة الانجليزية',
            'lang_name_en' => 'English',
        ]);
    }
}
