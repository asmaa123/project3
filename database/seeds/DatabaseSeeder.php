<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('users')->insert([
//            'name' => 'admin system',
//            'phone' => '0500000000',
//            'password' => bcrypt("secret"), // secret
//            'active' => 1,
//            'type' => 1,
//            'remember_token' => str_random(10),
//        ]);
        DB::table('roles')->insert([
            'name' => 'GeneralManager',
            'guard_name' => 'admin',
            'slug' => 'المدير العام'
        ]);

        DB::table('model_has_roles')->insert([
            'role_id' => 1,
            'model_type' => 'App\User',
            'model_id' => 1
        ]);

        $this->call([
            LanguagesTableSeeder::class,
            PermissionTableSeeder::class,
        ]);

    }
}
