<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class createNewUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ar_SA');
        foreach (range(1,20) as $index) {
            $user = \App\User::create([
                'email' => $faker->email,
                'name' => $faker->name,
                'password' => bcrypt(123456)
            ]);
            $profile = \App\Models\Profile::create([
                'acount_status_id' => \App\Models\Account_status::all()->random()->id,
                'type_sugers_id' => \App\Models\Type_suger::all()->random()->id,
                'phone' => $faker->e164PhoneNumber,
                'age' => $faker->numberBetween(20,60),
                'user_id' => $user->id,
                'gender' => 'male',
                'height' => $faker->numberBetween(160,190),
                'image' => '1563881842.jpg',
                'weight' => $faker->numberBetween(60,90),
                'date_injury' => $faker->date('Y-m-d', 'now')
            ]);
            $this->command->info('successfully: user is creted => '. $user->id);

        }
    }
}
