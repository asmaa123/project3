<?php

use App\Admin;
use Illuminate\Database\Seeder;

class createFirstAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminsCount = Admin::count();
        if(! $adminsCount) {
            DB::table('admins')->insert([
                'name' => 'First Admin',
                'email' => 'admin@example.com',
                'password' => bcrypt("secret"), // secret
                'remember_token' => str_random(10),
            ]);
            $this->command->info('successfully: the first Admin is created');
            return;
        }
        $this->command->error('oops, the first Admin is already exists');
        return;
    }
}
