<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	public function up()
	{
		Schema::create('orders', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->string('number');
			$table->date('date');
			$table->string('price');
			$table->text('status');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('orders');
	}
}