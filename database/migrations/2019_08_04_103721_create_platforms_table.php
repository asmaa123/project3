<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlatformsTable extends Migration {

	public function up()
	{
		Schema::create('platforms', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('store_id')->unsigned();
			$table->integer('catogery_id')->unsigned();
			$table->integer('image_id')->unsigned();
			$table->string('image');
			$table->text('name');
			$table->text('text');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('platforms');
	}
}