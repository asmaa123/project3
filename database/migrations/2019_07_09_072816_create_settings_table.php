<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingsTable extends Migration {

	public function up()
	{
		Schema::create('settings', function(Blueprint $table) {
			$table->increments('id');
			$table->string('slug', 191);
			$table->string('name', 255);
			$table->string('value',255);
			$table->string('type',255);
			$table->string('module', 191);
			$table->string('orderby');

		});
	}

	public function down()
	{
		Schema::drop('settings');
	}
}