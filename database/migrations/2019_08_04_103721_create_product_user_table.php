<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductUserTable extends Migration {

	public function up()
	{
		Schema::create('product_user', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('product_id');
			$table->integer('user_id');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('product_user');
	}
}