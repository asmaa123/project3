<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOffersTable extends Migration {

	public function up()
	{
		Schema::create('offers', function(Blueprint $table) {
			$table->increments('id');
			$table->string('image')->default(null);
			$table->text('title');
			$table->string('price');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('offers');
	}
}