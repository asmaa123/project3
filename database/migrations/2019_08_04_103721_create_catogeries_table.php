<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCatogeriesTable extends Migration {

	public function up()
	{
		Schema::create('catogeries', function(Blueprint $table) {
			$table->increments('id');
			$table->text('name');
			$table->string('image')->default(null);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('catogeries');
	}
}