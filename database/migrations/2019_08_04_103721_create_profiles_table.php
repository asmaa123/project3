<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProfilesTable extends Migration {

	public function up()
	{
		Schema::create('profiles', function(Blueprint $table) {
			$table->increments('id');

			$table->string('phone');
			$table->string('image');
			$table->string('gender');
            $table->integer('user_id')->unsigned();
			$table->date('date');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('profiles');
	}
}