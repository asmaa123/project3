<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
            $table->foreign('profile_id')->references('id')->on('profiles');
			$table->string('email')->default(null);
            $table->text('name')->default(null);
			$table->string('password')->default(null);
			$table->string('remember_token')->default(null);
			$table->timestamps();

		});
	}

	public function down()
	{
		Schema::drop('users');
	}
}