<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSlidersTable extends Migration {

	public function up()
	{
		Schema::create('sliders', function(Blueprint $table) {
			$table->increments('id');
			$table->string('image')->default(null);
			$table->text('title');
			$table->text('text');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('sliders');
	}
}