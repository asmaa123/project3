<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCartsTable extends Migration {

	public function up()
	{
		Schema::create('carts', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('product_id')->unsigned();
			$table->integer('store_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->integer('color_id')->unsigned();
			$table->string('gty');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('carts');
	}
}