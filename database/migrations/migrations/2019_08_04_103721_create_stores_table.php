<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoresTable extends Migration {

	public function up()
	{
		Schema::create('stores', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('catogery_id')->unsigned();
			$table->string('logo');
            $table->string('namee');
			$table->double('lng');
			$table->double('lat');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('stores');
	}
}