<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationsTable extends Migration {

	public function up()
	{
		Schema::create('notifications', function(Blueprint $table) {
			$table->increments('id');
			$table->string('object_id');
			$table->string('notifiable_id');
			$table->text('title');
			$table->string('action');
			$table->string('notifiable_type');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('notifications');
	}
}