<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('profiles', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('products', function(Blueprint $table) {
			$table->foreign('catogery_id')->references('id')->on('catogeries')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('products', function(Blueprint $table) {
			$table->foreign('offer_id')->references('id')->on('offers')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('contacts', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('messages', function(Blueprint $table) {
			$table->foreign('store_id')->references('id')->on('stores')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('messages', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('carts', function(Blueprint $table) {
			$table->foreign('product_id')->references('id')->on('products')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('carts', function(Blueprint $table) {
			$table->foreign('store_id')->references('id')->on('stores')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('carts', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('carts', function(Blueprint $table) {
			$table->foreign('color_id')->references('id')->on('colors')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('platforms', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('platforms', function(Blueprint $table) {
			$table->foreign('store_id')->references('id')->on('stores')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('platforms', function(Blueprint $table) {
			$table->foreign('catogery_id')->references('id')->on('catogeries')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('platforms', function(Blueprint $table) {
			$table->foreign('image_id')->references('id')->on('images')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('platform_user', function(Blueprint $table) {
			$table->foreign('pad_id')->references('id')->on('platforms')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('platform_user', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('orders', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('product_order', function(Blueprint $table) {
			$table->foreign('product_id')->references('id')->on('products')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('product_order', function(Blueprint $table) {
			$table->foreign('order_id')->references('id')->on('orders')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('colors', function(Blueprint $table) {
			$table->foreign('product_id')->references('id')->on('products')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('stores', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('stores', function(Blueprint $table) {
			$table->foreign('catogery_id')->references('id')->on('catogeries')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::table('profiles', function(Blueprint $table) {
			$table->dropForeign('profiles_user_id_foreign');
		});
		Schema::table('products', function(Blueprint $table) {
			$table->dropForeign('products_catogery_id_foreign');
		});
		Schema::table('products', function(Blueprint $table) {
			$table->dropForeign('products_offer_id_foreign');
		});
		Schema::table('contacts', function(Blueprint $table) {
			$table->dropForeign('contacts_user_id_foreign');
		});
		Schema::table('messages', function(Blueprint $table) {
			$table->dropForeign('messages_store_id_foreign');
		});
		Schema::table('messages', function(Blueprint $table) {
			$table->dropForeign('messages_user_id_foreign');
		});
		Schema::table('carts', function(Blueprint $table) {
			$table->dropForeign('carts_product_id_foreign');
		});
		Schema::table('carts', function(Blueprint $table) {
			$table->dropForeign('carts_store_id_foreign');
		});
		Schema::table('carts', function(Blueprint $table) {
			$table->dropForeign('carts_user_id_foreign');
		});
		Schema::table('carts', function(Blueprint $table) {
			$table->dropForeign('carts_color_id_foreign');
		});
		Schema::table('platforms', function(Blueprint $table) {
			$table->dropForeign('platforms_user_id_foreign');
		});
		Schema::table('platforms', function(Blueprint $table) {
			$table->dropForeign('platforms_store_id_foreign');
		});
		Schema::table('platforms', function(Blueprint $table) {
			$table->dropForeign('platforms_catogery_id_foreign');
		});
		Schema::table('platforms', function(Blueprint $table) {
			$table->dropForeign('platforms_image_id_foreign');
		});
		Schema::table('platform_user', function(Blueprint $table) {
			$table->dropForeign('platform_user_pad_id_foreign');
		});
		Schema::table('platform_user', function(Blueprint $table) {
			$table->dropForeign('platform_user_user_id_foreign');
		});
		Schema::table('orders', function(Blueprint $table) {
			$table->dropForeign('orders_user_id_foreign');
		});
		Schema::table('product_order', function(Blueprint $table) {
			$table->dropForeign('product_order_product_id_foreign');
		});
		Schema::table('product_order', function(Blueprint $table) {
			$table->dropForeign('product_order_order_id_foreign');
		});
		Schema::table('colors', function(Blueprint $table) {
			$table->dropForeign('colors_product_id_foreign');
		});
		Schema::table('stores', function(Blueprint $table) {
			$table->dropForeign('stores_user_id_foreign');
		});
		Schema::table('stores', function(Blueprint $table) {
			$table->dropForeign('stores_catogery_id_foreign');
		});
	}
}