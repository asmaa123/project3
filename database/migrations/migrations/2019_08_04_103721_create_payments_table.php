<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentsTable extends Migration {

	public function up()
	{
		Schema::create('payments', function(Blueprint $table) {
			$table->increments('id');
			$table->text('fullname');
			$table->string('number');
			$table->date('date');
			$table->string('type');
			$table->string('price');
			$table->string('total');
			$table->string('charges');
			$table->string('dicount');
			$table->text('title');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('payments');
	}
}