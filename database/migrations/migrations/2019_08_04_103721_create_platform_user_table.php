<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlatformUserTable extends Migration {

	public function up()
	{
		Schema::create('platform_user', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('pad_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('platform_user');
	}
}