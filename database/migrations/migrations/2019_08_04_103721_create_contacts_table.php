<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactsTable extends Migration {

	public function up()
	{
		Schema::create('contacts', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->text('name');
			$table->string('email');
            $table->integer('read_at');
			$table->text('content');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('contacts');
	}
}