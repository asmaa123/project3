<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRatingsTable extends Migration {

	public function up()
	{
		Schema::create('ratings', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('rateable_id')->unsigned();
			$table->string('rateable_type');
			$table->text('comment');
			$table->string('store_degree');
			$table->string('product_degree');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('ratings');
	}
}