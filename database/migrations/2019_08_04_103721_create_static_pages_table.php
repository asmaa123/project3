<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaticPagesTable extends Migration {

	public function up()
	{
		Schema::create('static_pages', function(Blueprint $table) {
			$table->increments('id');
			$table->string('image');
			$table->text('text');
            $table->text('name');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('static_pages');
	}
}