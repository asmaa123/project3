<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	public function up()
	{
		Schema::create('products', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('catogery_id')->unsigned();
			$table->integer('offer_id')->unsigned();
			$table->integer('type');
			$table->string('price');
			$table->text('details');
			$table->text('name');
			$table->string('discount_value')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('products');
	}
}