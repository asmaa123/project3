<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect('admincp');
});

Route::get('/test','\Users\Http\controllers\Backend\UserController@index');
Auth::routes();

#======================================= Admin Area ===========================================
#==============================================================================================

# ========== admin Auth Area ==============================================
Route::get('admincp/login','Admin\LoginController@showLoginForm');
Route::post('admincp/login','Admin\LoginController@login')->name('admin.login');
Route::post('admincp/password/email','Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::post('admincp/password/reset','Admin\ResetPasswordController@reset')->name('admin.password.update');
Route::get('admincp/password/reset','Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::get('admincp/password/reset/{token}','Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');
# =====================================================================

Route::group(['prefix' => 'admincp', 'middleware' => ['admin','auth:admin']], function () {

    Route::get('logout','Admin\LoginController@logout')->name('admin.logout');

    Route::get('/', function () {
        return view('admin.home.index');
    });


    // first controller to test standard pages
    Route::resource('testArea', 'Admin\FirstController');
    Route::get('testArea/{id}/delete', 'Admin\FirstController@destroy');

    #============================ Roles Area =================================
    Route::resource('roles', 'RoleController');
    Route::get('roles/users/{roleName}', 'RoleController@UsersHasRole');

    #============================ Users Area =================================
    Route::resource('users', 'UserController');

    #=========================== Site Settings Area ==================================
    Route::resource('offers','Admin\OfferController');

    Route::resource('orders','Admin\OrderController');

    Route::resource('images','Admin\ImageController');

    Route::resource('colors','Admin\ColorController');

    Route::resource('catogeries','Admin\CatogeryController');

    Route::resource('sliders','Admin\SliderController');

    Route::resource('settings','Admin\SettingsController');

    Route::resource('contact_us','Admin\ContactusController');

    Route::resource('user','Admin\UserController');

    Route::resource('questions','Admin\QuestionController');

    Route::resource('products','Admin\ProductController');

    Route::resource('pages','Admin\StaticpageController');

    Route::resource('admins','Admin\AdminController');

    Route::resource('contacts','Admin\ContactController');

    Route::resource('statistics','Admin\StatisticsController');

    Route::resource('admincp','Admin\StatisticsController');

    Route::resource('platforms','Admin\PlatformController');

    Route::resource('stores','Admin\StoreController');

    Route::resource('advertisements','Admin\StoreController');

});

Route::get('testtt', function () {
    return view('admin.testt');
});
