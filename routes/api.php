<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('login', 'UserController@login');
Route::post('register', 'UserController@register');


Route::group(['middleware' => 'auth:api'], function(){
Route::post('details', 'UserController@details');
});



Route::group(['prefix'=>'v1'],function() {


    Route::get('showblogs', 'BllogController@index');

    Route::get('detailblog', 'Blog2Controller@index');

    Route::Post('addcomment', 'CommentController@addcomment');

    Route::get('showcomment', 'CommentController@showcomment');

    Route::get('Diabeteseducation', 'DiabeteseController@index');

    Route::get('medicine', 'MedicineController@index');

    Route::get('catogeries', 'CatogeryController@index');

    Route::get('products', 'ProductController@index');

    Route::get('detailsproduct', 'ProductdController@index');

    Route::get('settings', 'SettingController@index');

    Route::get('conditions', 'ConditionController@index');

    Route::get('readings', 'ReadingController@index');

    Route::get('detailreadings', 'DetailreadingController@index');

    Route::Post('contact_us', 'Contact_usController@addcontact');

    Route::get('resetpassword', 'ResetPasswordController@index');

    Route::get('newpassword', 'NewPasswordController@index');



});
