<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Users\Providers\UserServiceProvider;

class AppServiceProvider extends UserServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
