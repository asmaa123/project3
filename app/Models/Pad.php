<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pad extends Model 
{

    protected $table = 'pads';
    public $timestamps = true;
    protected $fillable = array('image', 'name', 'text', 'user_id', 'store_id', 'image_id', 'catogery_id');

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function store()
    {
        return $this->belongsTo('App\Models\Store', 'store_id');
    }

    public function image()
    {
        return $this->belongsTo('App\Models\Image', 'image_id');
    }

    public function catogery()
    {
        return $this->belongsTo('App\Models\Catogery', 'catogery_id');
    }

}