<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model 
{

    protected $table = 'orders';
    public $timestamps = true;
    protected $fillable = array('user_id', 'number', 'date', 'price', 'status');

    public function products()
    {
        return $this->belongsToMany(Product::class,'product_order','order_id','product_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}