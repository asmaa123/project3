<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Platform extends Model 
{

    protected $table = 'platforms';
    public $timestamps = true;
    protected $fillable = array('user_id', 'store_id', 'catogery_id', 'image_id', 'image', 'name', 'text');

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function store()
    {
        return $this->belongsTo('App\Models\Store', 'store_id');
    }

    public function image()
    {
        return $this->morphTo();
    }

    public function catogery()
    {
        return $this->belongsTo('App\Models\Catogery', 'catogery_id');
    }

}