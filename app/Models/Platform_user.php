<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Platform_user extends Model 
{

    protected $table = 'platform_user';
    public $timestamps = true;
    protected $fillable = array('pad_id', 'user_id');

}