<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model 
{

    protected $table = 'profiles';
    public $timestamps = true;
    protected $fillable = array('user_id', 'phone', 'image', 'gender', 'date');

    public function user()
    {
        return $this->hasOne('App\Models\User');
    }

}