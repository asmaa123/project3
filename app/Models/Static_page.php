<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Static_page extends Model 
{

    protected $table = 'static_pages';
    public $timestamps = true;
    protected $fillable = array('image', 'text','name');

}