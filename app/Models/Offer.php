<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model 
{

    protected $table = 'offers';
    public $timestamps = true;
    protected $fillable = array('image', 'title', 'price');

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

}