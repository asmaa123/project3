<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Color extends Model 
{

    protected $table = 'colors';
    public $timestamps = true;
    protected $fillable = array('color');

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function carts()
    {
        return $this->hasMany('App\Models\Cart');
    }

}