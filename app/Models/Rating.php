<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model 
{

    protected $table = 'ratings';
    public $timestamps = true;
    protected $fillable = array('rateable_id', 'rateable_type', 'comment', 'store_degree', 'product_degree');

    public function store()
    {
        return $this->morphTo();
    }

    public function product()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->morphTo();
    }

}