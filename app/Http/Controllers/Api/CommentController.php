<?php

namespace App\Http\Controllers\Api;

use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    use apiResponseTrait;

    public function addcomment(Request $request){

        $rule=[
            'content'=>'required',
            'blog_id'=>'required',
            'user_id'=>'required'

        ];

        $validator=validator()->make($request->all(),$rule);

        if($validator->fails()){

            return responseJson(1,$validator->errors()->first(),$validator->errors());
        }


        $comment=Comment::create($request->all());


        return $this->apiResponse($comment);

    }

    public function showcomment(){

        $coments=Comment::all();

        return $this->apiResponse($coments);

    }


}
