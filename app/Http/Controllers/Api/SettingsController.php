<?php

namespace App\Http\Controllers\Api;

use App\Models\Language;
use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    use apiResponseTrait;
    /**
     * Display a listing of the languages.
     *
     * @return \Illuminate\Http\Response
     */
    public function languages()
    {
        $languages = Language::select('lang_id', 'lang_name_en AS name')->get();
        return $this->apiResponse($languages);
    }
    /**
     * Display a listing of the countries.
     *
     * @return json Response
     */
    public  function getCountries()
     {
         $countries=Country::select(['country_id','country_name_en AS country_name','country_phone_code'])->whereIsActive(1)->get()->toArray();
         return $this->apiResponse($countries);

     }
}
