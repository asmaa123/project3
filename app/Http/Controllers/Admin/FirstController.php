<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FirstController extends Controller
{
    public function index()
    {
        return view('admin.test.index');
    }

    public function create()
    {
        return view('admin.test.add');
    }

    public function edit()
    {
        $user = (object) ['id'=>1,'name'=>'shennawy','email'=>'test@test.com','job'=>'web developer','phone'=>'0126566566'];
        return view('admin.test.edit',compact('user'));
    }

    public function destroy()
    {
        return back()->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم الحذف بنجاح']));
    }

}
