<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Static_page;
use Intervention\Image\Facades\Image;

class StaticpageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $pages=Static_page::all();

       return view('admin.page.index',compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.page.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule=[
            'name'=>'required|max:191',
            'image'=>'required',
            'text'=>'required'

                ];

        $message=[

            'name.requied'=>'this is required',
            'image.requied'=>'this is required',
            'text.requied'=>'this is required'

               ];

        $this->validate($request,$rule,$message);

        $page=Static_page::create([
            'name'=> json_encode($request->name, JSON_UNESCAPED_UNICODE),
            'text'=> json_encode($request->text, JSON_UNESCAPED_UNICODE)

                                  ]);

        if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->resize(300, 300)->save( public_path('/uploads/' . $filename ) );
            $page->image = $filename;
            $page->save();
                                            };

        flash()->message('تم إضافة الصفحه بنجاح');

        return redirect(route('pages.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page=Static_page::find($id);

        return view('admin.page.edit',compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $page=Static_page::find($id);

        $page->update([
            'name'=> json_encode($request->name, JSON_UNESCAPED_UNICODE),
            'text'=> json_encode($request->text, JSON_UNESCAPED_UNICODE)
                   ]);


        if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->resize(200, 200)->save( public_path('/uploads/' . $filename ) );
            $page->image = $filename;
            $page->save();
                                            };


        flash()->message('تم تعديل الصفحه بنجاح');

        return redirect(route('pages.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page=Static_page::find($id);
        $page->delete();
        flash()->message('تم حذف الصفحه بنجاح');
        return back();
    }
}
