<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Profile;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $users=User::with('profile')->get();

        return view('admin.user.index',compact('users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rule=[
            'name'=>'required',
            'email'=>'required',
            'image'=>'required'

               ];

        $message=[

            'name.requied'=>'this is required',
            'email.requied'=>'this is required',
            'image.requied'=>'this is required'

                  ];

        $this->validate($request,$rule,$message);



        $cat=User::create($request->all());


        $profile= Profile::create($request->except('name', 'email','password'));


        if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->resize(200, 200)->save( public_path('/uploads/' . $filename ) );
            $profile->image = $filename;
            $profile->save();
        };



        $cat->profile()->save($profile);



        flash()->message('تم إضافة الممستخدم بنجاح');

        return redirect(route('user.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $prods=User::find($id);

        return view('admin.user.edit',compact('prods'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $prods=User::find($id);
        $prods->update($request->all());



        flash()->message('تم تعديل المستخدم بنجاح');

        return redirect(route('user.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat=User::find($id);
        $cat->delete();
        flash()->message('تم حذف المستخدم بنجاح');
        return back();
    }
}
