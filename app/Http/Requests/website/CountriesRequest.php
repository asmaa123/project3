<?php

namespace App\Http\Requests\website;

use Illuminate\Foundation\Http\FormRequest;

class CountriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_name_ar' => 'required|string',
            'country_name_en' => 'required|string',
            'country_code' => 'required|string',
            'country_phone_code' => 'required|numeric',
            'lang_id' => 'required'
        ];
    }
}
