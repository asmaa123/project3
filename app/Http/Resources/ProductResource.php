<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,

            'image' => $this->image,

            'name' => getApiJsonData($this->name,request()->header('accept-language')),

            'protine' => $this->protine,

            'calories' => $this->calories,

            'created_at' => $this->created_at,

            'updated_at' => $this->updated_at



        ];
    }
}
