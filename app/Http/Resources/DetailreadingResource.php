<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DetailreadingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,

            'measurement_id' => $this->measurement_id,

            'created_at' => $this->created_at,

            'time' => $this->time,

            'updated_at' => $this->updated_at



        ];
    }
}
